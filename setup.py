#!/usr/bin/python3
from setuptools import setup


setup(
    name='debrequest',
    version='0.2',
    scripts=['bin/debrequest'],
    author='Dmitry Bogatov',
    author_email='KAction@gnu.org',
    package_data={
        'debrequest': ['templates/*']
    },
    license='GPLv3+',
    packages=['debrequest']
    )
